from .path import DATA, LOC
import os
import json

class Dict(object):
    def __init__(self, data={}):
        self.__dict__ = {
                key:Dict(value)
                for key,value in data.items()
            }

    def __new__(cls, data={}):
        if isinstance(data, dict):
            instance = super(Dict, cls).__new__(cls)
            return instance
        elif isinstance(data, list):
            return [Dict(x) for x in data]
        else:
            return data
    
    def __repr__(self):
        return self.__dict__.__repr__()

    def __getitem__(self, key):
        return self.__dict__[key]
    
    def __setitem__(self, key, value):
        self.__dict__[key] = value
    
    def keys(self):
        return self.__dict__.keys()
    
    def items(self):
        return self.__dict__.items()

class Database:
    def __init__(self, data_path=DATA):
        self.data_path = data_path
        self.data = {}
        self.flat_data = {}
        self.load_data(self.data, self.data_path)
        self.link_data()

    def load_data(self, dic, path):
        for item in os.listdir(path):
            fp = os.path.join(path, item)
            if os.path.isfile(fp):
                with open(fp, "rb") as f:
                    data = json.loads(f.read())
                if len(data) == 1:
                    data = list(data.values())[0]
                if data and isinstance(data[0], dict):
                    data = [Dict(item) for item in data]
                    for key in ["id", "iname", "type"]:
                        if hasattr(data[0], key):
                            data = {getattr(item,key):item for item in data}
                            break
                dic[os.path.splitext(item)[0]] = data

            elif os.path.isdir(fp):
                dic[item] = {}
                self.load_data(dic[item], fp)
    
    def link_data(self):
        ret = {}
        for main, item in self.data.items():
            if isinstance(item, list):
                continue
            for key in item.keys():
                if key not in ret:
                    ret[key] = Dict()
                ret[key][main] = item[key]
        self.link = ret

    def load_flat_data(self, dic: dict = {}, main=False):
        # if not dic:
        #     dic = self.data

        # if isinstance(dic, dict):
        #     if 'iname' in dic:
        #         iname = dic['iname']
        #         if iname in self.flat_data:
        #             self.flat_data[iname].update(dic)
        #         else:
        #             self.flat_data[iname] = dic
        #     else:
        #         for value in dic.values():
        #             self.load_flat_data(value)
        # elif isinstance(dic, list):
        #     for item in dic:
        #         self.load_flat_data(item)
        # else:
        #     return
        # # auto-link keys to values to make the access easier
        # # thanks to python's pointer system this takes even less RAM than keeping the keys as string
        # if main:
        #     for item in self.flat_data.values():
        #         for key, val in item.items():
        #             if isinstance(val, str) and key != "iname" and val in self.flat_data:
        #                 item[key] = self.flat_data[val]
        pass

    def __getitem__(self, key):
        if key in self.data:
            return self.data[key]
        elif key in self.link:
            return self.link[key]


class Localization:
    def __init__(self, loc_path=LOC):
        self.loc_path = loc_path
        self.data = {}
        self.load_data(self.data, self.loc_path)
        self.loc = self.generate_loc()

    def load_data(self, dic, path):
        for item in os.listdir(path):
            fp = os.path.join(path, item)
            if os.path.isfile(fp):
                with open(fp, "rb") as f:
                    data = json.loads(f.read())
                if len(data) == 1:
                    data = data["infos"]
                dic[os.path.splitext(item)[0]] = data
            elif os.path.isdir(fp):
                self.load_data(dic, fp)
                #dic[item] = {}
                #self.load_data(dic[item], fp)
    
    def generate_loc(self):
        ret = {}
        for master, items in self.data.items():
            for item in items:
                key = item["key"]
                if key not in ret:
                    ret[key] = Dict()
                ret[key][master] = item["value"]
        return ret

    def __getitem__(self, key):
        if key in self.data:
            return self.data[key]
        elif key in self.loc:
            return Dict(self.loc[key])
        return None

LOC = Localization()
DB = Database()
