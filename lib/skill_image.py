import os
from PIL import Image
IMAGE_PATH = "D:\\Disassemlby\\WarOfTheVisions\\assets\\japan\\ui\\00_common\\sprites\\abilitydetail\cmn_range_icon_%s.png"

iatkrange   = Image.open(IMAGE_PATH%"attackrange")
iatkrange2  = Image.open(IMAGE_PATH%"attackrange2")
inone       = Image.open(IMAGE_PATH%"none")
irange      = Image.open(IMAGE_PATH%"range")
iself       = Image.open(IMAGE_PATH%"self")
itarget     = Image.open(IMAGE_PATH%"target")

class Grid:
    def __init__(self, size, ppb):
        # size (x,y)
        # p(ixel)p(er)b(lock)
        self.size = size
        #self.ppb = ppb
        self.grid = {
            (x,y) : [inone]
            for x in range(size[0])
            for y in range(size[1])
        }
        
    def image(self, ppb=14):
        img = Image.new("RGBA", (self.size[0]*ppb, self.size[1]*ppb), color = (0,0,0,255))
        for x in range(self.size[0]):
            for y in range(self.size[1]):
                for timg in self.grid[(x,y)]:
                    img.paste(timg, (x*ppb, y*ppb, (x+1)*ppb, (y+1)*ppb), timg)
        return img

    def show(self):
        self.image().show()

    def set(self, target, img):
        cell = self.grid[tuple(target)]
        if img == iatkrange and irange in cell:
            cell.remove(irange)
            cell.append(iatkrange2)
        elif img == irange and iatkrange in cell:
            cell.remove(iatkrange)
            cell.append(iatkrange2)
        else:
            cell.append(img)

    def set_all(self, img):
        for y in range(self.size[1]):
            for x in range(self.size[0]):
                self.set((x,y), img)

    def set_cross(self, target, length, img):
        x,y = target
        for i in range(length+1):
            self.set((x+i,y), img)
            self.set((x-i,y), img)
            self.set((x,y+i), img)
            self.set((x,y-i), img)

    def set_diamond(self, target, img):
        x,y = target
        rmax = 2
        rmin = 0
        for index1 in range(-rmax, rmax+1):
            for index2 in range(-rmax, rmax+1):
                if abs(index1) + abs(index2) <= rmax and (rmin <=0 or rmax <= 0 or abs(index1)+abs(index2) > rmin):
                    self.set((x+index2,y+index1), img)
        # self.set_cross((x+1,y), 1,img)
        # self.set_cross((x-1,y), 1,img)
        # self.set_cross((x,y+1), 1,img)
        # self.set_cross((x,y-1), 1,img)

        """
        for (int index1 = -max; index1 <= max; ++index1)
            for (int index2 = -max; index2 <= max; ++index2)
                if (Math.Abs(index2) + Math.Abs(index1) <= max && (min <= 0 || max <= 0 || Math.Abs(-index2) + Math.Abs(-index1) > min))
                    string str = (target.x + (float) index2).ToString() + "," + (target.y + (float) index1).ToString();
      """
    
    def set_horizontal_line(self, target, length, img):
        for i in range(length+1):
            self.set((target[0]-length//2+i,target[1]), img)
    
    def set_vertical_line(self, target, length, img):
        for i in range(length+1):
            self.set((target[0],target[1]-length//2+i), img)
    
    def set_square(self, target, length, img):
        left = target[0]-length
        upper = target[1]-length
        for y in range(length+2):
            for x in range(length+2):
                self.set((left+x,upper+y), img)

    def set_diagonal_cross(self, target, length, img):
        x,y = target
        for i in range(0,length+1):
            self.set((x+i,y+i), img)
            self.set((x-i,y+i), img)
            self.set((x-i,y-i), img)
            self.set((x-i,y-i), img)
    
    def set_laser(self, target, length, width, img):
        for y in range(length+1):
            for x in range(width+1):
                self.set((target[0]+x,target[1]+y), img)

    def set_type(self, skill, prefix, target, img):
        if prefix not in ["range", "eff"]:
            ValueError("invalid prefix")
        selection = skill[f"{prefix}_s"]

        length = skill[f"{prefix}_l"] if f"{prefix}_l" in skill else 1 
        width = skill[f"{prefix}_w"] if f"{prefix}_w" in skill else 1 
        if selection == 0: #Cross
            self.set_cross(target, length, img)
        elif selection == 1: #Diamond
            self.set_diamond(target, img)
        elif selection == 2: #Square
            self.set_square(target, length, img)
        elif selection == 3: #HoritontalLine
            NotImplementedError()
        elif selection == 4: #VerticalLine
            NotImplementedError()
        elif selection == 5: #DiagonalCross
            self.set_diagonal_cross(target, length, img)
        elif selection == 6: #SquareDiagonalCross
            NotImplementedError()
        elif selection == 10: #Laser
            self.set_laser(target, length, width, img)  
        elif selection == 11: #LaserTwin
            NotImplementedError()
        elif selection == 12: #LaserTriple
            NotImplementedError()
        elif selection == 13: #LaserSpread
            NotImplementedError()
        elif selection == 64: #All
            self.set_all(img)
        else:
            NotImplementedError()  

W,H = 17,17
PPB = 14
def gen_image(skill):
    g = Grid((W,H),PPB)

    # figure out the target tile
    # selection range
    if "range_s" in skill:
        g.set_type(skill, "range", (8,8), irange)

        #sel = skill[f"range_s"]
        target = (8, 8-skill["range_l"]+1)
    else:
        target = (8,8)

    # aoe effect
    if "eff_s" in skill:
        g.set_type(skill, "eff", target, iatkrange)



    # add target
    g.set(target, itarget)
    # add self
    g.set((8,8),iself)
    g.image().save("blizzara.png")
    #img.paste()
    print()

    # range_bns
    # range_buff
    # range_h
    # range_l
    # range_m
    # range_mh
    # range_s
    # range_w

blizzara = {
    	"range_s": 1,
		"range_l": 3,
		"range_h": 2,
		"eff_s": 0,
		"eff_l": 1,
		"eff_h": 1,
}
gen_image(blizzara)