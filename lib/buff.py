from .database import Dict, DB, LOC
from .enum import EBuffType, EBuffStatusParam, EBuffStatusCalc, EBuffEffectCondition, EBuffEffectTiming, EBuffEffectCheckTarget, EBuffEffectCheckTiming
from .consts import KILLER

class Buff(Dict):
    iname : int
    rate : int
    conds : list
    turn : int
    stats : dict
    chktgt : EBuffEffectCheckTarget
    chktiming : EBuffEffectCheckTiming
    timing : EBuffEffectTiming

    # calc/type/val?/val?1/tag/id

    def __new__(cls, iname):
        return super(Buff, cls).__new__(cls)

    def __init__(self, iname):
        if isinstance(iname, str):
            self.iname = iname
            buff = DB["Buff"][iname]
        else:
            self.iname = iname.iname
            buff = iname

        self.chktgt = EBuffEffectCheckTarget(buff.chktgt)
        self.chktiming = EBuffEffectCheckTiming(buff.chktiming)
        self.timing = EBuffEffectTiming(buff.timing)

        self.conds = [
            EBuffEffectCondition(cond)
            for cond in getattr(buff,"conds",[])
        ]

        # parse effects
        # on rare occurences the number in between can be missing, so we enumerate over the types via the keys
        self.effects = {}
        for key in buff.keys():
            if key[:4] != "type":
                continue
            num=key[4:]
            eff = Effect(buff[key], buff[f"calc{num}"], getattr(buff, f"val{num}", None), getattr(buff, f"val{num}1", None), getattr(buff, f"tag{num}", None))
            self.effects[eff.name] = eff



class Effect(Dict):
    typ  : EBuffStatusParam
    calc : EBuffStatusCalc
    vini : int
    vmax : int
    tags : list

    def __new__(cls, *args):
        return super(Effect, cls).__new__(cls)

    def __init__(self, typ, calc, vini, vmax, tags=[]):
        self.calc = EBuffStatusCalc(calc)
        self.typ = EBuffStatusParam(typ)
        self.vini = vini
        self.vmax = vmax
        self.tags = tags
    
    @property
    def name(self):
        name = ""
        if "Killer" not in self.typ.name:
            name = LOC[f"BUFF_{self.typ.name}"].buff
        else:
            kenum = KILLER[self.typ.name]
            ktyp = str(kenum)[8:-2] # not a good way, but ¯\_(ツ)_/¯
            if "Species" == ktyp:
                ktag = kenum(2**(self.tags[0]-100)).name
            else:
                ktag = kenum(self.tags[0]).name
            name = LOC[f"BUFF_Killer{ktyp}{ktag}"].buff
        
        suffix = LOC[f"""BUFF_{self.calc.name.replace("Rate","")}"""]
        if suffix:
            name += suffix.buff

        if "Rate" in self.calc.name:
            name += "(%)"
        
        return name


    def value_at(self, level:int, max_level:int):
        return int(self.vini + (self.vmax - self.vini)*(level-1)/(max_level-1))
    
    def __add__(self, stat2):
        return Effect(self.typ, self.calc, self.vini+stat2.vini, self.vmax+stat2.vmax, self.tags)