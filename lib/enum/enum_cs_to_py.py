import re

reEnum = re.compile(r"(public|private) enum (.+?)\n\{\n(.+?)\}", flags=re.S)
# 2 name
# 3 entries

reEntry = re.compile(r"\s*(.+?)( = (0x.+?))?,( \/\/ (.+))?")
# 1 name
# 3/5 num (hex)

with open("enums.cs", "rt", encoding="utf8") as f:
    text = f.read()

with open("__init__.py", "wt", encoding="utf8") as f:
    f.write("from enum import IntEnum\n")

    for _,name,entries in reEnum.findall(text):
        f.write(f"\nclass {name}(IntEnum):\n")
        for name, _, num1, _, num2 in reEntry.findall(entries):
            if name == "None":
                name = "NONE"
            f.write(f"\t{name} = {num1 if num1 else num2}\n")
       
print()