from enum import IntEnum

class EBuffCategory(IntEnum):
	CommandPassive = 0x0
	Command = 0x1
	VisionPassive = 0x2
	VisionParty = 0x3
	VisionPartyFriend = 0x4
	ArtifactPassive = 0x5
	Weather = 0x6
	Intimacy = 0x7
	Ground = 0x8
	Map = 0x9
	Max = 0xA

class EBuffStatusParam(IntEnum):
	NONE = 0x0
	BaseTop = 0x0
	Hp = 0x1
	Mp = 0x2
	Ap = 0x3
	Ct = 0x4
	BaseLast = 0x5
	StatusTop = 0x14
	Atk = 0x15
	Def = 0x16
	Mag = 0x17
	Mnd = 0x18
	Dex = 0x19
	Spd = 0x1A
	Luk = 0x1B
	Mov = 0x1C
	Jmp = 0x1D
	StatusLast = 0x1E
	ElemTop = 0x28
	ElemNone = 0x29
	ElemFire = 0x2A
	ElemIce = 0x2B
	ElemWind = 0x2C
	ElemEarth = 0x2D
	ElemThunder = 0x2E
	ElemWater = 0x2F
	ElemShine = 0x30
	ElemDark = 0x31
	ElemAll = 0x32
	ElemLast = 0x33
	AttackTop = 0x3C
	AttackSlash = 0x3D
	AttackPierce = 0x3E
	AttackBlow = 0x3F
	AttackShot = 0x40
	AttackMagic = 0x41
	AttackJump = 0x42
	AttackAll = 0x46
	AttackLast = 0x47
	CondTop = 0x50
	CondHealHp = 0x51
	CondHealMp = 0x52
	CondHealAp = 0x53
	CondPoison = 0x54
	CondBlinded = 0x55
	CondSleep = 0x56
	CondMute = 0x57
	CondParalysis = 0x58
	CondConfusion = 0x59
	CondCharm = 0x5A
	CondPetrification = 0x5B
	CondFrog = 0x5C
	CondChicken = 0x5D
	CondSneak = 0x5E
	CondHaste = 0x5F
	CondSlow = 0x60
	CondStop = 0x61
	CondStun = 0x62
	CondDontMove = 0x63
	CondDontAction = 0x64
	CondBerserk = 0x65
	CondDoom = 0x66
	CondRaise = 0x67
	CondReraise = 0x68
	CondProtect = 0x69
	CondShell = 0x6A
	CondReflec = 0x6B
	CondFaith = 0x6C
	CondInnocen = 0x6D
	CondLevitate = 0x6E
	CondDeath = 0x6F
	CondQuick = 0x70
	CondGuts = 0x71
	CondPhyPerfectAvoid = 0x72
	CondMagPerfectAvoid = 0x73
	CondPerfectHit = 0x74
	CondPerfectCritical = 0x75
	CondUnitTag = 0x76
	CondKillerElement = 0x77
	CondKillerSpecies = 0x78
	CondKillerBirth = 0x79
	CondKillerTag = 0x7A
	CondEnchant = 0x7B
	CondChangeCt = 0x7C
	CondKillerElementP = 0x7D
	CondKillerSpeciesP = 0x7E
	CondKillerBirthP = 0x7F
	CondKillerTagP = 0x80
	CondKillerElementM = 0x81
	CondKillerSpeciesM = 0x82
	CondKillerBirthM = 0x83
	CondKillerTagM = 0x84
	CondKillerElementDamage = 0x85
	CondKillerSpeciesDamage = 0x86
	CondKillerBirthDamage = 0x87
	CondKillerTagDamage = 0x88
	CondAllEsuna = 0x8C
	CondAllDespell = 0x8D
	CondAllStrenResist = 0x8E
	CondShield = 0x8F
	CondKillerSex = 0x90
	CondKillerSexP = 0x91
	CondKillerSexM = 0x92
	CondKillerSexDamage = 0x93
	CondLast = 0x94
	OtherTop = 0x96
	OtherInitAp = 0x97
	OtherEffectRange = 0x98
	OtherEffectScope = 0x99
	OtherEffectHeight = 0x9A
	OtherHit = 0x9B
	OtherAvoid = 0x9C
	OtherCrtRate = 0x9D
	OtherCrtHit = 0x9E
	OtherCrtAvoid = 0x9F
	OtherKillerElement = 0xAA
	OtherKillerSpecies = 0xAB
	OtherKillerBirth = 0xAC
	OtherKillerTag = 0xAD
	OtherKillerSex = 0xAE
	OtherHate = 0xB4
	OtherBrave = 0xB5
	OtherFaith = 0xB6
	OtherCastTime = 0xB7
	OtherApGain = 0xBE
	OtherSpGain = 0xBF
	OtherBraveBonus = 0xC0
	OtherFaithBonus = 0xC1
	OtherJpGain = 0xC2
	OtherDebuffResist = 0xC8
	OtherDebuffResistHp = 0xC9
	OtherDebuffResistAtk = 0xCA
	OtherDebuffResistDef = 0xCB
	OtherDebuffResistMag = 0xCC
	OtherDebuffResistMnd = 0xCD
	OtherDebuffResistDex = 0xCE
	OtherDebuffResistSpd = 0xCF
	OtherDebuffResistLuk = 0xD0
	OtherDebuffResistSlash = 0xE6
	OtherDebuffResistPierce = 0xE7
	OtherDebuffResistBlow = 0xE8
	OtherDebuffResistShot = 0xE9
	OtherDebuffResistMagic = 0xEA
	OtherDebuffResistHit = 0x104
	OtherDebuffResistAvoid = 0x105
	OtherDebuffResistBrave = 0x106
	OtherDebuffResistFaith = 0x107
	OtherBuffTurn = 0x12C
	OtherDebuffTurn = 0x12D
	OtherOneAttackResist = 0x136
	OtherRangeAttackResist = 0x137
	OtherDamageMax = 0x138
	OtherAttackSkillRank = 0x139
	OtherLast = 0x13A

class EBuffStatusCalc(IntEnum):
	NONE = 0x0
	Strength = 0x1
	StrengthRate = 0x2
	Resist = 0x3
	ResistRate = 0x4
	Heal = 0xA
	HealMaxRate = 0xB
	HealMagRate = 0xC
	Damage = 0x14
	DamageMaxRate = 0x15
	DamageRate = 0x16
	Grant = 0x1E
	Release = 0x1F
	ReleaseBuff = 0x20
	ReleaseDebuff = 0x21
	Invalid = 0x28

class ESelectType(IntEnum):
	All = 0x64
	Cross = 0x0
	Diamond = 0x1
	Square = 0x2
	HorizontalLine = 0x3
	VerticalLine = 0x4
	DiagonalCross = 0x5
	SquareDiagonalCross = 0x6
	Laser = 0xA
	LaserTwin = 0xB
	LaserTriple = 0xC
	LaserSpread = 0xD

class EMapEventType(IntEnum):
	NONE = 0x0
	Map = 0x1
	Atk = 0x2

class EAISkillCategory(IntEnum):
	NONE = 0x0
	Id = 0x1
	Damage = 0x2
	Heal = 0x3
	ConditionHeal = 0x4
	ConditionInvalid = 0x5
	BuffHeal = 0x6
	Revive = 0x7
	Buff = 0x8
	Debuff = 0x9
	DebuffHeal = 0xA
	Defence = 0xB
	Max = 0xC

class EAISkillCondition(IntEnum):
	NONE = 0x0
	LessHpHealRate15 = 0x2
	LessHpHealRate30 = 0x4
	LessHpHealRate45 = 0x8
	LessHpHealRate60 = 0x10
	LessHpHealRate75 = 0x20
	LessHpHealRate90 = 0x40
	LessMpHealRate15 = 0x80
	LessMpHealRate30 = 0x100
	LessMpHealRate45 = 0x200
	LessMpHealRate60 = 0x400
	LessMpHealRate75 = 0x800
	LessMpHealRate90 = 0x1000
	LessApHealRate15 = 0x2000
	LessApHealRate30 = 0x4000
	LessApHealRate45 = 0x8000
	LessApHealRate60 = 0x10000
	LessApHealRate75 = 0x20000
	LessApHealRate90 = 0x40000
	Hate = 0x10000000000
	Barrier = 0x20000000000
	MaskHpHeap = 0x7E
	MaskMpHeap = 0x1F80
	MaskApHeap = 0x7E000
	MaskOther = 0x30000000000

class EAISkillConditionOrder(IntEnum):
	NONE = 0x0
	Target = 0x1
	SkillDamageEffect = 0x2
	SkillHpHealEffect = 0x3
	SkillMpHealEffect = 0x4
	SkillApHealEffect = 0x5
	SkillReviveEffect = 0x6
	SkillBuffEffect = 0x7
	SkillDebuffEffect = 0x8
	SkillHealDebuffEffect = 0x9
	SkillHealCondEffect = 0xA
	SkillGoodCondEffect = 0xB
	SkillFailCondEffect = 0xC
	SkillInvalidCondEffect = 0xD
	Other1 = 0xE
	SkillHealBuffEffect = 0xF
	Max = 0x10

class EAITargetConditionFlag(IntEnum):
	NONE = 0x0
	RemoveCantTarget = 0x1
	InKnockOut = 0x2
	RemoveNotTarget = 0x4
	InSelf = 0x8
	InPetrification = 0x10
	RemoveDist = 0x20
	TargetAlly = 0x40
	TargetEnemy = 0x80
	TypeUnit = 0x100
	TypeHateUnit = 0x200
	TypeTreasure = 0x400
	TypeCrystal = 0x800
	TypeGimmick = 0x1000
	DistMaskBitTop = 0x1B
	DistMaskBit1 = 0x8000000
	DistMaskBit2 = 0x10000000
	DistMaskBit3 = 0x20000000
	DistMaskBit4 = 0x40000000
	DistMaskBit5 = 0xFFFFFFFF80000000
	MaskBase = 0x3F
	MaskTarget = 0xC0
	MaskType = 0x1F00
	MaskDist = 0xFFFFFFFFF8000000

class EAITargetCondition(IntEnum):
	NONE = 0x0
	IdUnit = 0x1
	IdTag = 0x2
	RollFront = 0x0
	RollMiddle = 0x4
	RollBack = 0x8
	LessDist = 0x0
	GreaterDist = 0x10
	LessHp = 0x0
	GreaterHp = 0x20
	LessHpRate25 = 0x40
	LessHpRate50 = 0x80
	LessHpRate75 = 0x100
	GreaterHpRate25 = 0x200
	GreaterHpRate50 = 0x400
	GreaterHpRate75 = 0x800
	LessMp = 0x0
	GreaterMp = 0x1000
	LessAp = 0x0
	GreaterAp = 0x2000
	LessAtk = 0x100000
	GreaterAtk = 0x200000
	LessDef = 0x400000
	GreaterDef = 0x800000
	LessMag = 0x1000000
	GreaterMag = 0x2000000
	LessMnd = 0x4000000
	GreaterMnd = 0x8000000
	LessDex = 0x10000000
	GreaterDex = 0x20000000
	LessSpd = 0x40000000
	GreaterSpd = 0x80000000
	LessLuk = 0x100000000
	GreaterLuk = 0x200000000
	LessHate = 0x0
	GreaterHate = 0x100000000000
	Dying = 0x200000000000
	Self = 0x400000000000
	Random = 0x800000000000
	MaskId = 0x3
	MaskDist = 0x10
	MaskRoll = 0xC
	MaskHp = 0xFE0
	MaskMp = 0x1000
	MaskAp = 0x2000
	MaskStatus = 0x3FFF00000
	MaskHate = 0x100000000000
	MaskOther = 0xE00000000000

class EAITargetConditionOrder(IntEnum):
	NONE = 0x0
	Id = 0x1
	Distance = 0x2
	Roll = 0x3
	Hp = 0x4
	Mp = 0x5
	Ap = 0x6
	Status = 0x7
	Hate = 0xE
	Other = 0xF
	Max = 0x10

class ECostPriority(IntEnum):
	NONE = 0x0
	MpHigh = 0x1
	ApHigh = 0x2

class EBattleResultType(IntEnum):
	NONE = 0x0
	Win = 0x1
	Lose = 0x2
	Draw = 0x3
	GiveUp = 0x4
	GameOver = 0x5

class EBattlePhase(IntEnum):
	NONE = 0x0
	StartDemo = 0x1
	Arrangement = 0x2
	Start = 0x3

class EBattleType(IntEnum):
	NONE = 0x0
	Story = 0x1
	MultiFreeMatch = 0x2
	MultiFriendMatch = 0x3
	MultiQuest = 0x4
	Event = 0x5
	Hard = 0x6
	NBeast = 0x7
	CharaQuest = 0x8
	Arena = 0x9
	MultiRaid = 0xA
	Tutorial = 0xB
	Beginner = 0xC
	Arrangement = 0xD
	MultiClassMatch = 0xE
	GuildBattle = 0xF
	Max = 0x10

class eMapUnitCtCalcType(IntEnum):
	FIXED = 0x0
	SCALE = 0x1

class EMapBreakAIType(IntEnum):
	NONE = 0x0
	ALL = 0x1
	PALL = 0x2
	EALL = 0x3

class EMapBreakSideType(IntEnum):
	UNKNOWN = 0x0
	PLAYER = 0x1
	ENEMY = 0x2

class EMapBreakRayType(IntEnum):
	PASS = 0x0
	TERRAIN = 0x1

class ColorBlendMode(IntEnum):
	NONE = 0x0
	Lerp = 0x1
	Blink = 0x2

class EHitReactionType(IntEnum):
	NONE = 0xFFFFFFFFFFFFFFFF
	Normal = 0x0

class EViewRestrictionType(IntEnum):
	Opened = 0x0
	Closed = 0x1
	Initialized = 0x2
	StartUp = 0x3
	Started = 0x4
	Open = 0x5
	Close = 0x6
	NotExist = 0x7
	Exist = 0x8

class EPlaceType(IntEnum):
	Normal = 0x0
	InList = 0x1
	AbsolutePathWithViewAsRoot = 0x2

class ECommandType(IntEnum):
	Description = 0x0
	Talk = 0x1
	GuidForUI = 0x2
	ViewOperationNextButton = 0x5
	TutorialEnd = 0x6
	ShowDialog = 0x7
	RestrictionWithView = 0x8
	RestrinctionWithAnimation = 0x9
	ViewOperationSemitransparent = 0xA
	BattleTrigger = 0xB
	BattleAction = 0xC
	ExecuteEndAPI = 0xD
	RedrawSelection = 0xE
	RestrictionWithCircleMotion = 0xF
	Highlight = 0x10
	Tips = 0x11
	TrackEvent = 0x12

class ESwitch(IntEnum):
	On = 0x0
	Off = 0x1

class ESearchTargetType(IntEnum):
	Self = 0x0
	Child = 0x1
	List = 0x2

class ESearchType(IntEnum):
	IName = 0x0
	ObjectName = 0x1

class EInamesDataType(IntEnum):
	NONE = 0x0
	Item = 0x1
	Unit = 0x2
	Artifact = 0x3
	Quest = 0x4
	VisionCard = 0x5
	Skill = 0x6
	Job = 0x7

class EDropType(IntEnum):
	NONE = 0xFFFFFFFFFFFFFFFF
	Item = 0x0
	Artifact = 0x1
	Gil = 0x2
	Lapis = 0x3
	PlayerExp = 0x4
	Unit = 0x6
	NBeast = 0x7
	Vision = 0x8
	Crystal = 0x63

class EGachaUseItemType(IntEnum):
	NONE = 0xFFFFFFFFFFFFFFFF
	Gil = 0x0
	Lapis = 0x1
	PaymentLapis = 0x2
	Ticket = 0x3
	Video = 0x4

class ETutorialGachaState(IntEnum):
	NONE = 0xFFFFFFFFFFFFFFFF
	Tutorial_StartGacha = 0x0
	Tutorial_End = 0x1

class GuildBadgeType(IntEnum):
	LoginReward = 0x258
	ReceiveGift = 0x259
	LeaderReward = 0x25A
	JoinRequest = 0x25B

class EPartySlotType(IntEnum):
	Free = 0x0
	Locked = 0x1
	Forced = 0x2
	ForcedHero = 0x3
	Npc = 0x4
	NpcHero = 0x5
	Other = 0x6

class EPartySlotIndex(IntEnum):
	Slot1 = 0x0
	Slot2 = 0x1
	Slot3 = 0x2
	Slot4 = 0x3
	Slot5 = 0x4
	SlotSupport = 0x5
	Max = 0x6

class EChatMessageType(IntEnum):
	Text = 0x1
	Stamp = 0x2
	Guild = 0x3
	Match = 0x4
	System = 0x2710

class EChatCategory(IntEnum):
	World = 0x0
	Room = 0x1
	Guild = 0x2

class ELimitedType(IntEnum):
	NONE = 0x0
	Once = 0x1
	Day = 0x2

class ESaleType(IntEnum):
	NONE = 0x0
	Gil = 0x1
	Lapis = 0x2
	Lapis_P = 0x4
	RealMoney = 0x8
	Item = 0x10

class SkillEffectTarget(IntEnum):
	Target = 0x0
	Self = 0x1

class EMapEffect(IntEnum):
	NONE = 0x0
	Cloud = 0x1
	Rain = 0x2
	Snow = 0x3
	SnowStorm = 0x4

class PartyCondType(IntEnum):
	NONE = 0x0
	Limited = 0x1
	Forced = 0x2

class WeaponFormulaTypes(IntEnum):
	NONE = 0x0
	Atk = 0x1
	Mag = 0x2
	AtkSpd = 0x3
	MagSpd = 0x4
	AtkDex = 0x5
	MagDex = 0x6
	AtkLuk = 0x7
	MagLuk = 0x8
	AtkMag = 0x9
	SpAtk = 0xA
	SpMag = 0xB
	AtkSpdDex = 0xC
	MagSpdDex = 0xD
	AtkDexLuk = 0xE
	MagDexLuk = 0xF
	Luk = 0x10
	Dex = 0x11
	Spd = 0x12
	Cri = 0x13
	Def = 0x14
	Mnd = 0x15
	AtkRndLuk = 0x16
	MagRndLuk = 0x17
	AtkEAt = 0x18
	MagEMg = 0x19
	AtkDefEDf = 0x1A
	MagMndEMd = 0x1B
	LukELk = 0x1C
	MHp = 0x1D

class EArtifactSet(IntEnum):
	NONE = 0xFFFFFFFFFFFFFFFF
	Equip1 = 0x0
	Equip2 = 0x1
	Trust = 0x2
	Max = 0x3

class EArtifactRank(IntEnum):
	E = 0x0
	D = 0x1
	C = 0x2
	B = 0x3
	A = 0x4
	S = 0x5

class ERecipeType(IntEnum):
	Item = 0x0
	Artifact = 0x1

class EBuffEffectCondition(IntEnum):
	NONE = 0x0
	SexUnkown = 0x1
	SexMem = 0x2
	SexWomen = 0x3
	ElemNone = 0xA
	ElemFire = 0xB
	ElemIce = 0xC
	ElemWind = 0xD
	ElemEarth = 0xE
	ElemThunder = 0xF
	ElemWater = 0x10
	ElemShine = 0x11
	ElemDark = 0x12
	BackStab = 0x13
	Dying = 0x14
	SideAlly = 0x15
	SideEnemy = 0x16
	SpeciesHuman = 0x1E
	SpeciesNetherBeast = 0x1F
	SpeciesBeast = 0x20
	SpeciesDemon = 0x21
	SpeciesDragon = 0x22
	SpeciesPlane = 0x23
	SpeciesBird = 0x24
	SpeciesInsect = 0x25
	SpeciesAquatic = 0x26
	SpeciesMachine = 0x31
	SpeciesSpirit = 0x28
	SpeciesUndead = 0x29
	SpeciesStone = 0x2A
	SpeciesMetal = 0x2B
	BirthUnknown = 0x3D
	BirthLeonis = 0x3E
	BirthHourn = 0x3F
	BirthFenice = 0x40
	BirthWezett = 0x41
	BirthCrystalFaith = 0x42
	BirthAllyare = 0x43
	BirthRundall = 0x44
	BirthCyga = 0x45
	BirthGuren = 0x46
	BirthOwis = 0x47
	BirthHyndra = 0x48
	BirthFFBE = 0x49
	BirthFF1 = 0x4A
	BirthFF2 = 0x4B
	BirthFF3 = 0x4C
	BirthFF4 = 0x4D
	BirthFF5 = 0x4E
	BirthFF6 = 0x4F
	BirthFF7 = 0x50
	BirthFF8 = 0x51
	BirthFF9 = 0x52
	BirthFF10 = 0x53
	BirthFF11 = 0x54
	BirthFF12 = 0x55
	BirthFF13 = 0x56
	BirthFF14 = 0x57
	BirthFF15 = 0x58
	BirthFFT = 0x59

class EBuffEffectTiming(IntEnum):
	QuestStart = 0x0
	SkillBefore = 0x1
	SkillAfter = 0x2
	SkillReaction = 0x3
	Weather = 0xA
	Move = 0x14

class EBuffEffectCheckTarget(IntEnum):
	Target = 0x0
	User = 0x1

class EBuffEffectCheckTiming(IntEnum):
	NONE = 0xFFFFFFFFFFFFFFFF
	Eternal = 0x0
	ActionStart = 0x1
	ActionEnd = 0x2
	SkillAfter = 0x6
	ClockCountUp = 0xA
	Weather = 0x14
	Conditional = 0x1E

class EGeoVariation(IntEnum):
	NONE = 0xFFFFFFFFFFFFFFFF
	Sun = 0x0
	Rain = 0x1
	Snow = 0x2
	Burn = 0x3
	Fire = 0x4
	Ice = 0x5
	Wind = 0x6
	Earth = 0x7
	Thunder = 0x8
	Water = 0x9
	Shine = 0xA
	Dark = 0xB
	Max = 0xC

class EJobObtain(IntEnum):
	NONE = 0x0
	Support1 = 0x1
	Support2 = 0x2

class EVisionCardType(IntEnum):
	NONE = 0xFFFFFFFFFFFFFFFF
	Equip = 0x0
	Exp = 0x1
	Sell = 0x2
	Max = 0x3

class EPartyLabelType(IntEnum):
	NONE = 0x0
	LEADER = 0x1
	MEMBER1 = 0x2
	MEMBER2 = 0x3
	MEMBER3 = 0x4
	MEMBER4 = 0x5
	MEMBER5 = 0x6
	OFFICIAL = 0x7
	FRIEND = 0x8
	HERO = 0x9
	SUPPORT = 0xA
	LOCK = 0xB
	PARTY1 = 0xC
	PARTY2 = 0xD
	PARTY3 = 0xE
	PARTY4 = 0xF
	PARTY5 = 0x10

class EArtifactStatusLabelType(IntEnum):
	DEFAULT = 0x0
	UP = 0x1

class EAdventureRapidType(IntEnum):
	Coin = 0x1
	Ticket_2 = 0xB
	Ticket_5 = 0xC
	Ticket_10 = 0xD

class EAdventureCampaignType(IntEnum):
	NONE = 0x0
	UnitExpUp = 0x1
	DropRareUp = 0x2
	ItemDropUp = 0x3
	SpeedupLimitUp = 0x4
	SpeedupCost = 0x5
	SpeedupDropExpUp = 0x6
	GillUp = 0x7

class GiftTypes(IntEnum):
	Item = 0x1
	Gil = 0x2
	Coin = 0x4
	ArenaCoin = 0x8
	MultiCoin = 0x10
	KakeraCoin = 0x20
	Artifact = 0x40
	Unit = 0x80
	SelectUnitItem = 0x100
	SelectItem = 0x200
	SelectArtifactItem = 0x400
	Award = 0x800

class CreateItemResult(IntEnum):
	NotEnough = 0x0
	CanCreate = 0x1
	CanCreateCommon = 0x2

class EQuestMissionType(IntEnum):
	Clear = 0x0
	DeadAll = 0x1
	NotDead = 0x2
	NotDead2 = 0x3
	DeadLess = 0x4
	ClearNotDead = 0x5
	PartyOnlyElement = 0xA
	PartyOnlyAttack = 0xB
	PartyOnlyArtifact = 0xC
	PartyOnlyBirth = 0xD
	PartyOnlySex = 0xE
	PartyInUnit = 0xF
	PartyOnlyUnit = 0x10
	PartyLess = 0x11
	BadDeadUnit = 0x14
	EscapeUnit = 0x15
	DeadUnitGreater = 0x16
	DeadUnitSameGreater = 0x17
	DeadUnitFromUnit = 0x18
	DeadBreakObjectLess = 0x1E
	DeadBreakObjectGreater = 0x1F
	DeadUnitSkill = 0x28
	UseSkill = 0x29
	UseSkillLess = 0x2A
	ActionGimmick = 0x32
	UseItemLess = 0x3C
	NotHireFriend = 0x46
	HireFriend = 0x47
	DeadEnemyGreater = 0x50
	DeadEnemySameGreater = 0x51
	HealHpLess = 0x52
	AttackNumLess = 0x53
	TakenDamageLess = 0x54
	DamageGreater = 0x55
	OneTimeDamgeGreater = 0x56
	LeaderActionCountLess = 0x5A
	PartyActionCountLess = 0x5B
	ContinueLess = 0x64
	TreasureGreater = 0x65
	CrystalLess = 0x66
	CrystalGreater = 0x67
	ChainGreater = 0x68
	ElementChainGreater = 0x69
	NotAuto = 0xC8
	OtherMissionSameDone = 0xC9

class KeyQuestTypes(IntEnum):
	NONE = 0x0
	Timer = 0x1
	Count = 0x2

class EStatusAttack(IntEnum):
	NONE = 0x0
	Slash = 0x1
	Pierce = 0x2
	Blow = 0x3
	Shot = 0x4
	Magic = 0x5
	Jump = 0x6

class EStatusBase(IntEnum):
	Hp = 0x0
	Mp = 0x1
	Ap = 0x2
	Atk = 0x3
	Def = 0x4
	Mag = 0x5
	Mnd = 0x6
	Dex = 0x7
	Spd = 0x8
	Luk = 0x9
	Mov = 0xA
	Jmp = 0xB
	Cost = 0xC

class EStatusCondition(IntEnum):
	NONE = 0x0
	HealHp = 0x1
	HealMp = 0x2
	HealAp = 0x3
	Poison = 0x4
	Blinded = 0x5
	Sleep = 0x6
	Mute = 0x7
	Paralysis = 0x8
	Confusion = 0x9
	Charm = 0xA
	Petrification = 0xB
	Frog = 0xC
	Chicken = 0xD
	Sneak = 0xE
	Haste = 0xF
	Slow = 0x10
	Stop = 0x11
	DontMove = 0x12
	DontAction = 0x13
	Berserk = 0x14
	Doom = 0x15
	Raise = 0x16
	Reraise = 0x17
	Protect = 0x18
	Shell = 0x19
	Reflec = 0x1A
	Faith = 0x1B
	Innocen = 0x1C
	Levitate = 0x1D
	Death = 0x1E
	Quick = 0x1F
	Guts = 0x20
	PhyPerfectAvoid = 0x21
	MagPerfectAvoid = 0x22
	PerfectHit = 0x23
	PerfectCritical = 0x24
	UnitTag = 0x25
	KillerElement = 0x26
	KillerSpecies = 0x27
	KillerBirth = 0x28
	KillerTag = 0x29
	Stun = 0x2A
	Enchant = 0x2B
	ChangeCt = 0x2C
	KillerElementP = 0x2D
	KillerSpeciesP = 0x2E
	KillerBirthP = 0x2F
	KillerTagP = 0x30
	KillerElementM = 0x31
	KillerSpeciesM = 0x32
	KillerBirthM = 0x33
	KillerTagM = 0x34
	KillerDamage = 0x35
	KillerSex = 0x36
	KillerSexP = 0x37
	KillerSexM = 0x38

class EElement(IntEnum):
	Unknown = 0xFFFFFFFFFFFFFFFF
	NONE = 0x0
	Fire = 0x1
	Ice = 0x2
	Wind = 0x3
	Earth = 0x4
	Thunder = 0x5
	Water = 0x6
	Shine = 0x7
	Dark = 0x8

class EElementPriority(IntEnum):
	Skill = 0x0
	Unit = 0x1

class EStatusOther(IntEnum):
	InitAp = 0x0
	EffectRange = 0x1
	EffectScope = 0x2
	EffectHeight = 0x3
	Hit = 0x4
	Avoid = 0x5
	CrtRate = 0x6
	CrtHit = 0x7
	CrtAvoid = 0x8
	KillerElement = 0x9
	KillerSpecies = 0xA
	KillerBirth = 0xB
	KillerTag = 0xC
	Hate = 0xD
	Brave = 0xE
	Faith = 0xF
	CastTime = 0x10
	ApGain = 0x11
	SpGain = 0x12
	BraveBonus = 0x13
	FaithBonus = 0x14
	DebuffResist = 0x15
	DebuffResistHp = 0x16
	DebuffResistAtk = 0x17
	DebuffResistDef = 0x18
	DebuffResistMag = 0x19
	DebuffResistMnd = 0x1A
	DebuffResistDex = 0x1B
	DebuffResistSpd = 0x1C
	DebuffResistLuk = 0x1D
	DebuffResistSlash = 0x1E
	DebuffResistPierce = 0x1F
	DebuffResistBlow = 0x20
	DebuffResistShot = 0x21
	DebuffResistMagic = 0x22
	DebuffResistHit = 0x23
	DebuffResistAvoid = 0x24
	DebuffResistBrave = 0x25
	DebuffResistFaith = 0x26
	BuffTurn = 0x27
	DebuffTurn = 0x28
	OneAttackResist = 0x29
	RangeAttackResist = 0x2A
	DamageMax = 0x2B
	AttackSkillRank = 0x2C
	KillerSex = 0x2D

class EStatusParam(IntEnum):
	NONE = 0x0
	Hp = 0x1
	Mp = 0x2
	Ap = 0x3
	Atk = 0x4
	Def = 0x5
	Mag = 0x6
	Mnd = 0x7
	Dex = 0x8
	Spd = 0x9
	Luk = 0xA
	Mov = 0xB
	Jmp = 0xC
	ElemNoneAssist = 0xD
	ElemFireAssist = 0xE
	ElemIceAssist = 0xF
	ElemWindAssist = 0x10
	ElemEarthAssist = 0x11
	ElemThunderAssist = 0x12
	ElemWaterAssist = 0x13
	ElemShineAssist = 0x14
	ElemDarkAssist = 0x15
	ElemNoneResist = 0x16
	ElemFireResist = 0x17
	ElemIceResist = 0x18
	ElemWindResist = 0x19
	ElemEarthResist = 0x1A
	ElemThunderResist = 0x1B
	ElemWaterResist = 0x1C
	ElemShineResist = 0x1D
	ElemDarkResist = 0x1E
	AttackSlashAssist = 0x1F
	AttackPierceAssist = 0x20
	AttackBlowAssist = 0x21
	AttackShotAssist = 0x22
	AttackMagicAssist = 0x23
	AttackJumpAssist = 0x24
	AttackSlashResist = 0x25
	AttackPierceResist = 0x26
	AttackBlowResist = 0x27
	AttackShotResist = 0x28
	AttackMagicResist = 0x29
	AttackJumpResist = 0x2A
	CondHealHpAssist = 0x2B
	CondHealMpAssist = 0x2C
	CondHealApAssist = 0x2D
	CondPoisonAssist = 0x2E
	CondBlindedAssist = 0x2F
	CondSleepAssist = 0x30
	CondMuteAssist = 0x31
	CondParalysisAssist = 0x32
	CondConfusionAssist = 0x33
	CondCharmAssist = 0x34
	CondPetrificationAssist = 0x35
	CondFrogAssist = 0x36
	CondSneakAssist = 0x37
	CondHasteAssist = 0x38
	CondSlowAssist = 0x39
	CondStopAssist = 0x3A
	CondDontMoveAssist = 0x3B
	CondDontActionAssist = 0x3C
	CondBerserkAssist = 0x3D
	CondDoomAssist = 0x3E
	CondRaiseAssist = 0x3F
	CondReraiseAssist = 0x40
	CondProtectAssist = 0x41
	CondShellAssist = 0x42
	CondReflecAssist = 0x43
	CondFaithAssist = 0x44
	CondInnocenAssist = 0x45
	CondLevitateAssist = 0x46
	CondDeathAssist = 0x47
	CondQuickAssist = 0x48
	CondGutsAssist = 0x49
	CondPhyPerfectAvoidAssist = 0x4A
	CondMagPerfectAvoidAssist = 0x4B
	CondPerfectHitAssist = 0x4C
	CondPerfectCriticalAssist = 0x4D
	CondUnitTagAssist = 0x4E
	CondKillerElementAssist = 0x4F
	CondKillerSpeciesAssist = 0x50
	CondKillerBirthAssist = 0x51
	CondKillerTagAssist = 0x52
	CondHealHpResist = 0x53
	CondHealMpResist = 0x54
	CondHealApResist = 0x55
	CondPoisonResist = 0x56
	CondBlindedResist = 0x57
	CondSleepResist = 0x58
	CondMuteResist = 0x59
	CondParalysisResist = 0x5A
	CondConfusionResist = 0x5B
	CondCharmResist = 0x5C
	CondPetrificationResist = 0x5D
	CondFrogResist = 0x5E
	CondSneakResist = 0x5F
	CondHasteResist = 0x60
	CondSlowResist = 0x61
	CondStopResist = 0x62
	CondDontMoveResist = 0x63
	CondDontActionResist = 0x64
	CondBerserkResist = 0x65
	CondDoomResist = 0x66
	CondRaiseResist = 0x67
	CondReraiseResist = 0x68
	CondProtectResist = 0x69
	CondShellResist = 0x6A
	CondReflecResist = 0x6B
	CondFaithResist = 0x6C
	CondInnocenResist = 0x6D
	CondLevitateResist = 0x6E
	CondDeathResist = 0x6F
	CondQuickResist = 0x70
	CondGutsResist = 0x71
	CondPhyPerfectAvoidResist = 0x72
	CondMagPerfectAvoidResist = 0x73
	CondPerfectHitResist = 0x74
	CondPerfectCriticalResist = 0x75
	CondUnitTagResist = 0x76
	CondKillerElementResist = 0x77
	CondKillerSpeciesResist = 0x78
	CondKillerBirthResist = 0x79
	CondKillerTagResist = 0x7A
	CondStun = 0x7B
	CondChangeCt = 0x7C
	OtherInitAp = 0x7D
	OtherEffectRange = 0x7E
	OtherEffectScope = 0x7F
	OtherEffectHeight = 0x80
	OtherHit = 0x81
	OtherAvoid = 0x82
	OtherCrtRate = 0x83
	OtherCrtHit = 0x84
	OtherCrtAvoid = 0x85
	OtherKillerElement = 0x86
	OtherKillerSpecies = 0x87
	OtherKillerBirth = 0x88
	OtherKillerTag = 0x89
	OtherHate = 0x8A
	OtherBrave = 0x8B
	OtherFaith = 0x8C
	OtherCastTime = 0x8D
	OtherDebuffResist = 0x8E
	OtherDebuffResistHp = 0x8F
	OtherDebuffResistAtk = 0x90
	OtherDebuffResistDef = 0x91
	OtherDebuffResistMag = 0x92
	OtherDebuffResistMnd = 0x93
	OtherDebuffResistDex = 0x94
	OtherDebuffResistSpd = 0x95
	OtherDebuffResistLuk = 0x96
	OtherDebuffResistSlash = 0x97
	OtherDebuffResistPierce = 0x98
	OtherDebuffResistBlow = 0x99
	OtherDebuffResistShot = 0x9A
	OtherDebuffResistMagic = 0x9B
	OtherDebuffResistHit = 0x9C
	OtherDebuffResistAvoid = 0x9D
	OtherDebuffResistBrave = 0x9E
	OtherDebuffResistFaith = 0x9F
	OtherBuffTurn = 0xA0
	OtherDebuffTurn = 0xA1
	OtherOneAttackResist = 0xA2
	OtherRangeAttackResist = 0xA3
	OtherDamageMax = 0xA4
	OtherAttackSkillRank = 0xA5

class EAISensing(IntEnum):
	All = 0x0
	NONE = 0x1
	SelfSide = 0x2
	EnemySide = 0x3

class EBirth(IntEnum):
	NONE = 0x0
	Unknown = 0x1
	Leonis = 0x2
	Hourn = 0x3
	Fenice = 0x4
	Wezett = 0x5
	CrystalFaith = 0x6
	Allyare = 0x7
	Rundall = 0x8
	Cyga = 0x9
	Guren = 0xA
	Owis = 0xB
	Hyndra = 0xC
	FFBE = 0xD
	FF1 = 0xE
	FF2 = 0xF
	FF3 = 0x10
	FF4 = 0x11
	FF5 = 0x12
	FF6 = 0x13
	FF7 = 0x14
	FF8 = 0x15
	FF9 = 0x16
	FF10 = 0x17
	FF11 = 0x18
	FF12 = 0x19
	FF13 = 0x1A
	FF14 = 0x1B
	FF15 = 0x1C
	FFT = 0x1D
	Goga = 0x1E
	Reserve01 = 0x36
	Reserve02 = 0x37
	Reserve03 = 0x38
	Reserve04 = 0x39
	Reserve05 = 0x3A
	Reserve06 = 0x3B
	Reserve07 = 0x3C
	Reserve08 = 0x3D
	Reserve09 = 0x3E
	Reserve10 = 0x3F

class EJobSet(IntEnum):
	NONE = 0xFFFFFFFFFFFFFFFF
	Main = 0x0
	Sub1 = 0x1
	Sub2 = 0x2
	Max = 0x3

class EJobType(IntEnum):
	NONE = 0x0
	Attacker = 0x1
	Defender = 0x2
	Healer = 0x3
	Supporter = 0x4

class ERoleType(IntEnum):
	NONE = 0x0
	Zenei = 0x1
	Tyuei = 0x2
	Kouei = 0x3

class ESex(IntEnum):
	NONE = 0x0
	Male = 0x1
	Female = 0x2
	Unknown = 0x3

class EAbilitySlot(IntEnum):
	NONE = 0x0
	Main = 0x1
	Sub = 0x2
	Support = 0x3
	Reaction = 0x4
	NetherBeast = 0x5
	Limit = 0x6
	Master = 0x7
	Party = 0x8

class ESkillType(IntEnum):
	NormalAttack = 0x0
	CommandAction = 0x1
	CommandReaction = 0x2
	CommandPassive = 0x3
	ArtifactAction = 0x4
	ArtifactReaction = 0x5
	ArtifactPassive = 0x6
	VisionAction = 0x7
	VisionReaction = 0x8
	VisionPassive = 0x9
	Leader = 0xA
	Item = 0xB
	Status = 0xC

class ESkillEffectType(IntEnum):
	NONE = 0x0
	Defence = 0x1
	Attack = 0x2
	Absorb = 0x3
	Heal = 0xA
	ConditionHeal = 0xB
	ConditionInvalid = 0xC
	BuffHeal = 0xD
	DebuffHeal = 0xE
	Revive = 0x14

class EAttackType(IntEnum):
	NONE = 0x0
	PhyAttack = 0x1
	MagAttack = 0x2
	PhyMagAttack = 0x3

class EAttackBase(IntEnum):
	NONE = 0x0
	Atk = 0x1
	Def = 0x2
	Mag = 0x3
	Mnd = 0x4
	Dex = 0x5
	Spd = 0x6
	Luk = 0x7
	AtkMag50x50 = 0x14
	HpSelf = 0x32
	HpTarget = 0x33
	MHpSelf = 0x34
	MHpTarget = 0x35
	MpSelf = 0x36
	MpTarget = 0x37
	MMpSelf = 0x38
	MMpTarget = 0x39
	ApSelf = 0x3A
	ApTarget = 0x3B
	MApSelf = 0x3C
	MApTarget = 0x3D
	CtSelf = 0x3E
	CtTarget = 0x3F
	Value = 0x64
	KillerValue = 0x65
	Damage = 0x66

class EAttackFormula(IntEnum):
	Default = 0x0
	BraveFaith = 0x1

class ESkillCostType(IntEnum):
	Ap = 0x0
	Mp = 0x1
	MpAp = 0x2

class EDamageType(IntEnum):
	NONE = 0x0
	AllDamage = 0x1
	PhyDamage = 0x2
	MagDamage = 0x3

class ESkillParamValueTarget(IntEnum):
	Hp = 0x0
	Mp = 0x1
	Ap = 0x2
	Atk = 0x3
	Def = 0x4
	Mag = 0x5
	Mnd = 0x6
	Hit = 0x7
	Dex = 0x8
	Spd = 0x9
	Avd = 0xA
	Crt = 0xB
	Crta = 0xC

class EBuffType(IntEnum):
	Buff = 0x0
	Debuff = 0x1

class ELineType(IntEnum):
	NONE = 0x0
	Line = 0x1
	Parabola = 0x2

class ESkillTarget(IntEnum):
	Self = 0x0
	NotSelf = 0x1
	SelfSide = 0xA
	EnemySide = 0xB
	All = 0xC
	KnockOutSelfSide = 0x14
	KnockOutEnemySide = 0x15
	KnockOutAll = 0x16

class ESkillTiming(IntEnum):
	Used = 0x0
	Passive = 0x1
	Wait = 0x2
	Dead = 0x3
	DamageControl = 0x4
	Reaction = 0x5
	FirstReaction = 0x6
	Dying = 0x7

class ESkillCondition(IntEnum):
	NONE = 0x0

class ECastType(IntEnum):
	NONE = 0x0
	Chant = 0x1
	Jump = 0x2

class ETargetLockType(IntEnum):
	Target = 0x0
	GridUnitLock = 0x1
	UnitLock = 0x2
	GridLock = 0x3

class ESkillMoveType(IntEnum):
	NONE = 0x0
	Move = 0x1
	MoveToSkill = 0x2
	SkillToMove = 0x3

class ESelfExpType(IntEnum):
	NONE = 0x0
	RestHpRate = 0x1

class EBarrierType(IntEnum):
	NONE = 0x0
	Guard = 0x1
	Life = 0x2
	Count = 0x3
	CountLife = 0x4
	Turn = 0x5
	TurnLife = 0x6

class EKnockBackDir(IntEnum):
	Back = 0x0
	Forward = 0x1

class EKnockBack(IntEnum):
	Self = 0x0
	Grid = 0x1

class ESkillFlag(IntEnum):
	ExecuteChant = 0x0
	SelfTargetSelect = 0x1
	PierceAttack = 0x2
	CastBreak = 0x3
	FixedDamage = 0x4
	FixedDamageKiller = 0x5
	MuteTarget = 0x6
	AreaSkill = 0x7
	Laser = 0x8
	Changing = 0x9
	PhyAttack = 0xA
	MagAttack = 0xB
	CalcBraveFaith = 0xC
	CalcNBeastBraveFaith = 0xD
	EnableRangeBuff = 0xE
	EnableEffectBuff = 0xF
	ForceEquip = 0x10
	CameraDirection = 0x11
	ForceDuplicate = 0x12

class ESpecies(IntEnum):
	NONE = 0x0
	Human = 0x2
	NetherBeast = 0x4
	Beast = 0x8
	Demon = 0x10
	Dragon = 0x20
	Plane = 0x40
	Bird = 0x80
	Insect = 0x100
	Aquatic = 0x200
	Machine = 0x400
	Spirit = 0x800
	Undead = 0x1000
	Stone = 0x2000
	Metal = 0x4000
	Reserve01 = 0x400000
	Reserve02 = 0x800000
	Reserve03 = 0x1000000
	Reserve04 = 0x2000000
	Reserve05 = 0x4000000
	Reserve06 = 0x8000000
	Reserve07 = 0x10000000
	Reserve08 = 0x20000000
	Reserve09 = 0x40000000
	Reserve10 = 0xFFFFFFFF80000000

class ESize(IntEnum):
	Grid1x1 = 0x0
	Grid2x2 = 0x1
	Grid3x3 = 0x2
	Max = 0x3
	Grid5x2 = 0xA
	Grid2x5 = 0xB

class EPersonal(IntEnum):
	NONE = 0x0
	Fine = 0x1
	Cool = 0x2
	Sexy = 0x3
	Weak = 0x4
	Neat = 0x5
	Mnly = 0x6
	Max = 0x7

class EUnitType(IntEnum):
	Unit = 0x0
	NetherBeast = 0x1
	Treasure = 0x2
	Crystal = 0x3
	Gem = 0x4
	EventUnit = 0x5
	GimmickObj = 0x6
	Enemy = 0x7
	EnemyNetherBeast = 0x8
	BadStatus = 0x9
	PlacedObj = 0xA
	Party = 0x80
	Arena = 0x81
	Max = 0x82

class EUnitTag(IntEnum):
	NONE = 0x0
	Floating = 0x2
	IgnoreGround = 0x4

class EMultiUnitState(IntEnum):
	ActStart = 0x0
	ActEnd = 0x1
	Move = 0x2
	Moved = 0x3
	MoveEnd = 0x4
	MoveCancel = 0x5
	DecDir = 0x6
	TurnEnd = 0x7
	CheckData = 0x8

class EMultiStampType(IntEnum):
	NONE = 0xFF
	Greeting_01 = 0x0
	Greeting_02 = 0x1
	Greeting_03 = 0x2
	Greeting_04 = 0x3
	Greeting_05 = 0x4
	Greeting_06 = 0x5
	Greeting_07 = 0x6
	Greeting_08 = 0x7

class EUnitSide(IntEnum):
	Player = 0x0
	Enemy = 0x1
	Neutral = 0x2

class EUnitDeadType(IntEnum):
	Knockout = 0x0
	KnockoutLeaveCorps = 0x1
	Dead = 0x2
	DeadLeaveCorps = 0x3
	Indomitable = 0x4
	Invincible = 0x5
	KnockoutOnly = 0x6

class EUnitFlag(IntEnum):
	Entried = 0x1
	Dead = 0x2
	ForceAuto = 0x4
	ActionProhibition = 0x8
	Searched = 0x10
	NoTarget = 0x20
	FixDir = 0x40
	SkillTarget = 0x80
	Damage = 0x100
	PartyMember = 0x200
	Escape = 0x400
	GimmickAction = 0x800
	Support = 0x1000
	FriendSupport = 0x2000
	DispHpGauge = 0x4000
	BlocktoUnit = 0x8000
	MoveProhibition = 0x10000
	HasDamageAnim = 0x20000
	Current = 0x80000
	Moved = 0x100000
	Action = 0x200000
	PredictMoved = 0x400000
	PredictAction = 0x800000
	SideAttack = 0x1000000
	BackAttack = 0x2000000
	KnockOut = 0x4000000
	ExecuteAI = 0x8000000
	ForceWait = 0x10000000
	Paralysis = 0x20000000
	ActReraise = 0x40000000

class EUnitCommandFlag(IntEnum):
	NONE = 0x0
	Move = 0x1
	Action = 0x2

class EUnitDirection(IntEnum):
	PositiveX = 0x0
	PositiveY = 0x1
	NegativeX = 0x2
	NegativeY = 0x3
	Auto = 0x4

class ERaidUnitType(IntEnum):
	NONE = 0x0
	EnemyNormal = 0x1
	EnemyBoss = 0x2

class EGridLayer(IntEnum):
	Unit = 0x0
	Moveable = 0x1
	MoveableCenter = 0x2
	Shatei = 0x3
	Kouka = 0x4
	KoukaSub = 0x5
	PlusEffect = 0x6
	MinusEffect = 0x7
	Arrangement = 0x8
	Debug = 0x9

class LANG_OPTION_TYPE(IntEnum):
	LANGUAGE = 0x0
	VO = 0x1

class EItemFilerType(IntEnum):
	Piece = 0x0
	Unit = 0x1
	Artifact = 0x2
	NetherBeast = 0x3
	Guild = 0x4
	Ticket = 0x5
	Expendables = 0x6
	Other = 0x7

class RecommendType(IntEnum):
	Total = 0x0
	Attack = 0x1
	Magic = 0x2
	Speed = 0x3
	AttackTypeSlash = 0x4
	AttackTypeStab = 0x5
	AttackTypeBlow = 0x6
	AttackTypeShot = 0x7
	AttackTypeMagic = 0x8
	AttackTypeNone = 0x9
	Hp = 0xA

class ESellItemsFilterType(IntEnum):
	Unit = 0x0
	Artifact = 0x1
	NetherBeast = 0x2
	Guild = 0x3
	Ticket = 0x4
	Expendables = 0x5
	Other = 0x6

class EStatusParamKey(IntEnum):
	NONE = 0x0
	Hp = 0x1
	Mp = 0x2
	Ap = 0x3
	Atk = 0x4
	Def = 0x5
	Mag = 0x6
	Mnd = 0x7
	Spd = 0x8
	Dex = 0x9
	Brave = 0xA
	Faith = 0xB
	Mov = 0xC
	Jmp = 0xD
	Range = 0xE
	Luk = 0xF
	Cost = 0x10
	OtherHit = 0x11
	OtherAvoid = 0x12
	OtherCrtHit = 0x13
	OtherCrtAvoid = 0x14
	OtherRange = 0x15
	OtherCost = 0x16
	AttackSlash = 0x17
	AttackPierce = 0x18
	AttackBlow = 0x19
	AttackShot = 0x1A
	AttackMagic = 0x1B
	ElemFire = 0x1C
	ElemIce = 0x1D
	ElemWind = 0x1E
	ElemEarth = 0x1F
	ElemThunder = 0x20
	ElemWater = 0x21
	ElemShine = 0x22
	ElemDark = 0x23
	CondPoison = 0x24
	CondBlinded = 0x25
	CondSleep = 0x26
	CondMute = 0x27
	CondParalysis = 0x28
	CondConfusion = 0x29
	CondPetrification = 0x2A
	CondFrog = 0x2B
	CondCharm = 0x2C
	CondSlow = 0x2D
	CondStop = 0x2E
	CondDontMove = 0x2F
	CondDontAction = 0x30
	CondBerserk = 0x31
	CondDoom = 0x32
