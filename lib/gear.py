from .database import DB, LOC, Dict
from .consts import RARITY, STATS
from .funcs import scale_stat, transform_buff
from .gamepedia import table

class Gear(Dict):
    def __new__(cls, iname):
        return super(Gear, cls).__new__(cls)

    def __init__(self, iname):
        self.__dict__.update(DB["Artifact"][iname].__dict__)
        self.__dict__.update(LOC[iname].__dict__)

    def gamepedia(self,wiki):
        if not hasattr(self, "equip") or "+" in self.artifactname:
            return
        if self.equip == "AEC_ACCE":
            gear_type = "accessory"
        elif self.equip in ["AEC_CLOTH", "AEC_HAT", "AEC_HELM", "AEC_SIELD"]:
            gear_type = "protect"
        else:
            gear_type = "weapon"

        # fill in the template
        wiki.pages[self.artifactname].save("\n".join(f"""
        {{{{Equipment
        |id={self.asset.lower()}
        |name={self.artifactname}
        |rarity={RARITY[self.rare].lower()}
        |lore={self.artifactflvr}
        |gear_type={gear_type}
        |equip_type=
        |}}}}
        """.replace('\u3000',"").split('\n        ')))
        
        # create the stat table
        rand = DB[self.rtype].ArtifactRandLot
        keys = self.status[0].keys()
        stats = []

        i=1
        while hasattr(rand.lot[0], f"grow{i}"):
            grow = DB[rand.lot[0][f"grow{i}"]].Grow
            stats.append([LOC[grow.type].artifactgrow,*[
                scale_stat(grow, grow.curve[0].lv, self.status[0][key], self.status[1 if hasattr(self.status[1], key) else 0][key], key)
                for key in keys
            ]])
            i+=1

        # wiki.pages[f"{self.artifactname}/stats"].save(table(
        #     ["Type", *[STATS[s] for s in self.status[0].keys()]],
        #     stats
        # ))

        # skill +
        if hasattr(self, "skl5"):
            skills = [DB[self.skl5[0]].Skill, *[
                    DB[DB[f"{self.iname}_{i}"].Artifact.skl5[0]].Skill
                    for i in range(1,6)
                    if DB[f"{self.iname}_{i}"]
                ]
            ]
            buffs  = [
                transform_buff(DB[getattr(skill,"s_buffs" if hasattr(skill, "s_buffs") else "t_buffs")[0]].Buff)
                for skill in skills
            ]
            keys = list(buffs[-1].keys())

            wiki.pages[f"{self.artifactname}/skill"].save(table(
                ["+", *keys],
                [
                    [
                        i,
                        *[
                            scale_stat(skills[i].grow,grow.curve[0].lv,buff[key][0] if key in buff else 0, buff[key][1] if key in buff else 0)
                            for key in keys
                        ]
                    ]
                    for i, buff in enumerate(buffs)
                ]
            ))

        # ability
        
            