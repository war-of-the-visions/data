from .database import DB, LOC
from .enum import EBuffStatusParam, EBuffStatusCalc
from .consts import KILLER

def scale_stat(grow, lv, sini, smax, typ = None):
    """Calculates the stat value at a given level
    :param grow: growth rate
    :param lv: target level
    :param sini: initial stat value
    :param smax: max stat value
    :param typ: stat type for modular grow
    """
    if isinstance(grow, str):
        grow = DB[grow]["Grow"]
    mlv = grow.curve[0].lv

    if lv == 1:
        ret = sini
    elif lv == mlv:
        ret = smax
    else:
        ret = int(sini + ((smax-sini) / (mlv-1) * (lv-1)))
    return int(ret * (1+grow.curve[0][typ]/100)) if typ and hasattr(grow.curve[0], typ) else ret 

def get_skill_buffs(skill):
    ret = []
    for typ in ["s_buffs", "t_buffs"]:
        ret.extend(getattr(skill, typ, []))
    return ret

# calc 1 - Increase target value - flat value - BUFF_Buff
# calc 2 - Increase target value - value % of max - BUFF_Buff
# calc 3 - raise attack/elem resistance - flat value - BUFF_Resist

# calc 10 - recover - flat value
# calc 11 - recover - value % of max
# calc 12 - recover - % of stat - formula

# calc 21 - dot dmg - % of stat - formula
# calc 22 - reduce target value by % value (reduce CT by 50%)

# calc 30 - inflict debuff val1-val11 & rate - kinda weird
# calc 31 - removes debuff - no rate or val - BUFF_Release
# calc 32 - removes all buffs - BUFF_ReleaseBuff
# calc 40 - nullify debuff for x turns - BUFF_Invalid

# ???
# BUFF_Debuff
#BUFF_ReleaseDebuff

def transform_buff(buff, ret_string=False):
    ret = {}
    if isinstance(buff, str):
        buff = DB["Buff"][buff]
    for i in range(0,20):
        if hasattr(buff, f"type{i}"):
            typ = EBuffStatusParam(buff[f"type{i}"])
            calc= EBuffStatusCalc(buff[f"calc{i}"])
            vali= getattr(buff,f"val{i}",100)
            valm= getattr(buff,f"val{i}1",100)
            tags = getattr(buff,f"tag{i}",[])

            name = ""
            if "Killer" not in typ.name:
                try:
                    name = LOC[f"BUFF_{typ.name}"].buff
                except:
                    name = typ.name
            else:
                kenum = KILLER[typ.name]
                ktyp = str(kenum)[8:-2] # not a good way, but ¯\_(ツ)_/¯
                if "Species" == ktyp:
                    ktag = kenum(2**(tags[0]-100)).name
                else:
                    ktag = kenum(tags[0]-1).name
                name = LOC[f"BUFF_Killer{ktyp}{ktag}"].buff
            
            suffix = LOC[f"""BUFF_{calc.name.replace("Rate","")}"""]
            if suffix:
                name += suffix.buff

            if "Rate" in calc.name:
                name += "(%)"
            

            ret[name] = (vali, valm)

    return ret