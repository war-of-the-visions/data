from .database import DB, LOC, Dict
from .consts import RARITY
from .funcs import scale_stat, transform_buff, get_skill_buffs
from .gamepedia import table
from .buff import Buff

class VisionCard(Dict):
    def __new__(cls, iname):
        return super(VisionCard, cls).__new__(cls)

    def __init__(self, iname):
        self.__dict__.update(DB["VisionCard"][iname].__dict__)
        self.__dict__.update(LOC[iname].__dict__)

        # figure out the stat growth rate via the self_buff
        self.grow = DB[self.self_buff]["Skill"].grow
    
    def gamepedia(self,wiki):
        levels = AWAKENING_TABLE[self.rare]
        # get self buff values
        self_types,self_vals = self.get_gamepedia_buff_vals("self_buff")
        group_types, group_vals = self.get_gamepedia_buff_vals("card_skill")

        self_stats = '\n'.join(f'''
            |sb{i+1}_name = {self_types[i]}
            |sb{i+1}_val_min = {self_vals[0][i][0]}
            |sb{i+1}_val_max  = {self_vals[0][i][1]}
            |sb{i+1}_a_min = {self_vals[1][i][0]}
            |sb{i+1}_a_max = {self_vals[1][i][1]}
            |sb{i+1}_mlv = {self_vals[2][i][1]}
            '''
            for i in range(len(self_types))
        )
        # fill in the template
        wiki.pages[self.visioncardname].save("\n".join(f"""
        {{{{VisionCard2
        |id={self.icon.lower()}
        |name={self.visioncardname}
        |cost={self.cost}
        |rarity={RARITY[self.rare].lower()}
        |lore={self.visioncardflvr}
        |lv1 = {levels[0]}
        |lv2 = {levels[1]}
        |lv3 = {levels[2]}
        |lv4 = {levels[3]}
        |lv5 = {levels[4]}
        |hp_min = {self.status[0]['hp']}
        |hp_max = {self.status[1]['hp']}
        |atk_min = {self.status[0]['atk']}
        |atk_max = {self.status[1]['atk']}
        |mag_min = {self.status[0]['mag']}
        |mag_max = {self.status[1]['mag']}
        {self_stats}
        |gb1_name = {group_types[0]}
        |gb1_val_min = {group_vals[0][0][0]}
        |gb1_val_max  = {group_vals[0][0][1]}
        |gb1_a_min = {group_vals[1][0][0]}
        |gb1_a_max = {group_vals[1][0][1]}
        |gb1_mlv = {group_vals[2][0][1]}
        |}}}}
        """.replace('\u3000',"").split('\n        ')))
        # create big table
        
    def discord(self):
        # generate values
        """mstats = [
            [
                scale_stat(self.grow, level, self.status[0][key], self.status[1][key])
                for key in ["hp","atk","mag"]
            ]
            for level in levels
        ]
        self_keys,self_buff = generate_stat_table(self.self_buff, getattr(self, "add_self_buff_awake", None), getattr(self, "add_self_buff_lvmax", None), levels)
        group_keys,group_buff = generate_stat_table(self.card_skill, getattr(self, "add_card_skill_buff_awake", None), getattr(self, "add_card_skill_buff_lvmax", None), levels)
        
        # generate big table
        sub_stat_table = "\n|-\n|".join([
            ' || '.join([levels[i]], *mstats[i], *self_buff[1:], *group_buff[1:])
            for i in range(5)
        ])
        wiki.pages[f"{self.visioncardname}/stats"].save(f'''{{| class="wikitable"
!rowspan="2"|LV
!colspan="3"|Stats
!colspan="{len(self_keys)-1}"|Self Buff
!colspan="{len(group_keys)-1}"|Group Buff
|-
! {" !! ".join(["HP","ATK","MAG", *self_keys[1:], *group_keys[1:]])}
|-
|{sub_stat_table}}}'''
        )
        print()
        """

    def get_gamepedia_buff_vals(self, key):
        skills = [
            getattr(self, key),
            getattr(self, f"add_{key.replace('_buff','')}_buff_awake", None),
            getattr(self, f"add_{key.replace('_buff','')}_buff_lvmax", None),
        ]
        buffs = []
        for skill in skills:
            if not skill:
                buffs.append({})
                continue
            skill = DB[skill].Skill
            buffs.append({effect.name : effect for effect in Buff(get_skill_buffs(skill)[0]).effects.values()})
        
        types = list(set(list(buffs[0].keys())+list(buffs[1].keys())+list(buffs[2].keys())))
        vals = [
            [
                (buff[typ].vini, buff[typ].vmax) if typ in buff else (0,0)
                for typ in types
            ]
            for buff in buffs
        ]
        return types, vals

        

def generate_stat_table(normal, awakening, lvmax, levels):
    # input - inames
    nskill = DB[normal].Skill
    ngrow = DB[nskill.grow].Grow
    nbuff = transform_buff(DB[getattr(nskill,"s_buffs" if hasattr(nskill, "s_buffs") else "t_buffs")[0]].Buff)
    # awakening
    if awakening:
        askill = DB[awakening].Skill
        agrow = DB[askill.grow].Grow
        abuff = transform_buff(DB[getattr(askill,"s_buffs" if hasattr(askill, "s_buffs") else "t_buffs")[0]].Buff)
    else:
        abuff = {}
    # max level
    if lvmax:
        mskill = DB[lvmax].Skill
        mbuff = transform_buff(DB[getattr(mskill,"s_buffs" if hasattr(mskill, "s_buffs") else "t_buffs")[0]].Buff)
    else:
        mbuff = {}

    keys = set(list(nbuff.keys())+list(abuff.keys())+list(mbuff.keys() if lvmax else []))
    return [
        ["LV",*[key for key in keys]],
        [
            *[
                [
                    lv,
                    *[
                        scale_stat(ngrow, lv, *nbuff[key]) + (scale_stat(agrow, i, *abuff[key]) if key in abuff else 0)
                        for key in keys
                    ]
                ]
                for i,lv in enumerate(levels[:-1])
            ],
            [
                levels[-1],
                *[
                    (mbuff[key][1] if key in mbuff else 0) + (abuff[key][1] if key in abuff else 0) + nbuff[key][1]
                    for key in keys
                ]
            ]
        ]
    ]

        #

AWAKENING_TABLE = {
    #N
    0: [10,15,20,25,30],
    #R
    1: [20,25,30,35,40],
    #SR
    2: [20,30,40,50,60],
    #SSR
    3: [30,40,50,60,70],
    #UR
    4: [40,55,70,85,99]
}