def table(headers=[],items=[[]]):
	return '{| class="wikitable"\n|-\n%s\n%s\n|}'%(
		'! %s'%' !! '.join([str(header) for header in headers]),
		'\n'.join(['|-\n|%s'%'\n| '.join([str(sitem) for sitem in sitems]) for sitems in items])
	)

def tabber(items):
	# items : (name, path)
	return '\n'.join([
		'<div class="tabbertab-borderless"><tabber>',
		*[
			'|-|{name} = {{{{:{path}}}}}'.format(name=name,path=path)
			for name, path in items
		],
		'</tabber></div>'
	])

def tooltip(text):
	return f'{{{{#tip-info: {text}}}}}'