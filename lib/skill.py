from .database import DB, LOC, Dict
from .enum import ESkillEffectType, ESkillTiming, ESkillType, EAbilitySlot, ESkillCostType, ESkillTarget, ELineType
from .buff import Buff

class Skill(Dict):
    # main keys
    eff_type : ESkillEffectType
    iname : str
    timing : ESkillTiming
    typ : ESkillType

    # optional keys
    # atk_base
    # atk_det
    # atk_formula
    atk_formula_t1 : int
    atk_formula_t2 : int
    atk_formula_t3 : int
    # atk_rev
    # atk_type
    # barrier
    # cam_dir
    # cap
    # chang
    # collaboType
    # combo_num
    # combo_rate

    cost_ap : int
    cost_mp : int
    cost_type : ESkillCostType
    
    count : int

    # crt_hit
    # crt_value

    # ct_lock
    # ct_spd
    # ct_spd1
    # ct_type
    # ctbreak
    
    # def_shi
    # def_wep
    
    # dupli
    
    # eff_dst
    # eff_h
    # eff_l
    # eff_rate
    # eff_rate1
    # eff_s
    # eff_val
    # eff_val1
    # effnm
    # elem
    # elem_pri
    exp : bool  # flag -self explosion
    # force_eq
    # gdupli
    # grow
    # hit
    # hp_bonus
    # hp_cost
    # hp_cost_rate
    # invtag
    # ischa
    # kback
    # klsp
    # klspr
    line : ELineType
    lvup_cost_rate : int
    # motnm
    # move
    # movie
    pierce : bool   # flag
    # range_bns
    # range_buff
    # range_h
    # range_l
    # range_m
    # range_mh
    # range_s
    # range_w
    rate : int
    # react_d_type
    # scn
    # selfsel
    slot : EAbilitySlot
    # stl_val
    # stl_val1
    target : ESkillTarget
    wth : dict # produces weather

    def __new__(cls, iname):
        return super(Skill, cls).__new__(cls)

    def __init__(self, iname):
        self.iname = iname
        # load loc
        loc = LOC[iname]
        self.name = loc.skillname
        self.expr = loc.skillexpr
        self.chant = loc.skillchant

        # load data
        skill = DB["Skill"][iname]
        self._skill = skill

    @property
    def s_buffs(self):
        return [Buff(buff) for buff in self._skill.s_buffs] if getattr(self._skill, "s_buffs", None) else []

    @property
    def t_buffs(self):
        return [Buff(buff) for buff in self._skill.t_buffs] if getattr(self._skill, "t_buffs", None) else []
    
    @property
    def buffs(self):
        return self.s_buffs+self.t_buffs
      