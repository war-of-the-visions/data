from .database import DB, LOC, Dict
from .consts import RARITY
from .funcs import scale_stat, transform_buff
from .gamepedia import table

class Esper(Dict):
    def __new__(cls, iname):
        return super(Esper, cls).__new__(cls)

    def __init__(self, iname):
        self.__dict__.update(DB["Unit"][iname].__dict__)
        self.__dict__.update(LOC[iname].__dict__)
        self.__dict__.update(DB["NetherBeastAbilityBoard"][iname].__dict__)
        self.__dict__.update(DB["NBeastAwakeRecipe"][iname].__dict__)
        self.status_awakened = DB["Unit"][self.nb_awake_id[0]].status
    
    def gamepedia(self, wiki):
        # Stats
        stats = '\n'.join([
            f"|{key}_min = {self.status[0][key]}\n|{key}_max = {self.status[1][key]}"
            for key in self.status[1].keys()
        ])
        stats_a = '\n'.join([
            f"|{key}_min_a = {self.status_awakened[0][key]}\n|{key}_max_a = {self.status_awakened[1][key]}"
            for key in self.status_awakened[1].keys()
        ])
        # Panels -> Abilities
        abilities = {}
        for panel in self.panels:
            buff = transform_buff(panel["value"])
            bkey = list(buff.keys())[0]
            bval = buff[bkey][1]
            if bkey not in abilities:
                abilities[bkey] = {}
            abilities[bkey][panel["panel_id"]] = {
                "sp" : panel["sp"],
                "val": bval
            }
        
        ability_tabber = "\n".join([
            '<div class="tabbertab-borderless"><tabber>',
            '\n'.join([
                f"""|-|{typ} = {table(headers = ["SP","Effect"], items = [
                    [
                        panel["sp"], f"+{panel['val']} {typ}"
                    ]
                    for panel in panels.values()
                ])}"""
                for typ, panels in abilities.items()
            ]),
		    '</tabber></div>'
        ])
        
        text = f"""{{{{Esper
|id={self.charaId.lower()}
|name={self.unitname}
|cost={self.cost}
|rarity={RARITY[self.rare]}
|lore={self.unitexpr}
{stats}
{stats_a}
|}}}}
{ability_tabber}
"""
        wiki.pages[self.unitname].save(text)
