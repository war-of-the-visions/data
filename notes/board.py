from PIL import Image, ImageDraw
import os
import math
import json
import re

reSkill = re.compile(r"SK_LW_(.+?)_(.+?)_.+")
# 1. job
# 2. type - S - SubSkill, M - Main Skill, P - Passive, R - Reaction

IMAGE_SRC = "D:\\Disassemlby\\WarOfTheVisions\\assets\\japan\\ui\\03_home\\ability\\sprites\\abilityboardview"
JOB_SRC = "D:\\Disassemlby\\WarOfTheVisions\\assets\\images\\job"
UNIT_SRC = "D:\\Disassemlby\\WarOfTheVisions\\assets\\images\\unit icon"

IMAGES = {}
for f in os.listdir(IMAGE_SRC):
    fp = os.path.join(IMAGE_SRC, f) 
    IMAGES[f.split("_",2)[2][:-4]] = Image.open(fp)

JOBS = {}
def get_job(name):
    global JOBS
    if name not in JOBS:
        JOBS[name] = Image.open(os.path.join(JOB_SRC, f"{name}.png"))
    return JOBS[name]


UNITS = json.load(open("D:\\Disassemlby\\WarOfTheVisions\\data_usage\\res\\data\\Unit.json", "rt", encoding="utf8"))["items"]
UNIT_BOARDS = json.load(open("D:\\Disassemlby\\WarOfTheVisions\\data_usage\\res\\data\\UnitAbilityBoard.json", "rt", encoding="utf8"))["items"]


def main():
    l = Layout(UNIT_BOARDS[0])
    l.generate_nodes(4)
    l.generate_image(160).save("t.png")

class Node(object):
    def __init__(self, x,y):
        self.x = x
        self.y = y
        self.neighbors = []

    @property
    def pos(self):
        return (self.x, self.y)
    
    def __mul__(self, val):
        return (self.x*val, self.y*val)

class Layout(object):
    def __init__(self, board):
        self.nodes = [Node(0,0)]
        self.lines = []
        self.board = board

    def generate_nodes(self, levels):
        self.levels = levels
        for level in range(1,levels+1):
            pts = []
            # calculate the edges
            # https://www.quora.com/How-can-you-find-the-coordinates-in-a-hexagon
            edges = [
                (
                    (level*math.cos(math.radians(i*60))), 
                    (level*math.sin(math.radians(i*60))),
                )
                for i in range(-1,5) #starting at 1 for the upper right corner as start
            ] 
            for i in range(6):
                p1 = edges[i]
                p2 = edges[(i+1)%6]

                # calculate the positions between the edges
                for j in range(0,level):
                    pts.append(Node(
                        p1[0] + (p2[0]-p1[0])*(j/level),
                        p1[1] + (p2[1]-p1[1])*(j/level)
                    ))
            self.nodes.extend(pts[-int(level/2):]+pts[:-int(level/2)])

    def generate_image(self, spacing, debug=False):
        img = Image.new("RGBA", (round(self.levels * spacing * 4+ 100), round(self.levels * spacing * 3)), color=0x000000FF)

        # set mid image
        unit = UNITS[0]
        unit_img = Image.open(os.path.join(UNIT_SRC, f'{unit["charaId"].lower()}_m.png'))
        add_img(img, unit_img.resize((spacing, spacing)), (0,0))

        for panel in self.board["panels"]:
            self.set_unit_panel(img, panel, unit, spacing)

        return img
    
    def set_unit_panel(self, img, panel, unit, spacing):
        node = self.nodes[panel["panel_id"]]

        if panel["panel_effect_type"] == 2:
            typ = "status"
            styp = ""
            job = unit["jobsets"][panel["applicable_job"] - 1].split('_')[2]
        else:
            job, styp = reSkill.findall(panel["value"])[0]
            if styp in ["M","S"]:
                # need check if main or sub :unamused:
                #if panel["get_job"] == 1 and (panel["jp"] == 0 or panel["need_level"] == 12):
                #    styp = "M"
                #else:
                #    styp = "S"

                # there are no indicators to determine if a skill is a main or sub skill,
                # so let's just assume, that specific panels are ALWAYS main skills
                # so 
                if panel["panel_id"] in [37, 42, 47]:
                    styp = "M"
                else:
                    styp = "S"
                
            typ = SKILL_TYPE[styp]

        add_img(img, IMAGES[f"cover_unit_{typ}_release"],node*spacing, panel["panel_id"])
        if styp == "M":
            add_img(img, IMAGES["main_base_on"], node*spacing)
        #else:
        #    add_img(img, IMAGES["sub_base"], node*spacing)

        # base glow as to be colored and probably resized?
        #add_img(img, IMAGES["base_glow"], node*spacing)
        add_img(img, IMAGES[f"type_unit_{typ}"], node*spacing)
        if job:
            add_img(img, get_job(job), node*spacing)
        
        #add_img(img, IMAGES["light"], node*spacing)
        
        


SKILL_TYPE = {
    "S" : "action",
    "M" : "action",
    "R" : "reaction",
    "P" : "support"
}


def add_img(img, add, rel_pos, text=""):
    center = (
        round(img.size[0]/2+rel_pos[0]-add.size[0]/2), 
        round(img.size[1]/2+rel_pos[1]-add.size[1]/2)
    )
    
    img.paste(add, center, add)
    draw = ImageDraw.Draw(img)
    draw.text(center, str(text), fill=(0,0,0,255), size=80)

#def draw_line(img, pt1, pt2):
        # line = [
        #     (round(pt[0]+img.size[0]//2),round(pt[1]+img.size[1]//2))
        #     for pt in line
        # ]
        # draw.text(
        # (
        #     int(line[0][0]+(line[1][0]-line[0][0])*5/11),
        #     int(line[0][1]+(line[1][1]-line[0][1])*5/11)
        # ), str(i), fill=(255,255,255), size=80)

main()