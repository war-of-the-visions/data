import json
import os

from lib import DB, LOC
from lib.database import Dict
from lib.funcs import transform_buff
comp = []
for iname,beast in DB["NetherBeastAbilityBoard"].items():
    ns = beast.__dict__
    ns["panels"] = {
        i : {
            "panel_id" : item.panel_id,
            "value" : transform_buff(item.value),
            "sp" : item.sp 
        }
        for i,item in enumerate(ns["panels"]) 
    }
    ns["lines"] = {i:item.line_id for i,item in enumerate(ns["lines"])}
    comp.append(ns)

with open("esper_board.json","wt",encoding="utf8") as f:
    json.dump(comp, f, ensure_ascii=False, indent="\t")
