import json
import os

from lib import DB, LOC
from lib.database import Dict
comp = []
for iname,skill in DB["Skill"].items():
    ns = skill.__dict__
    for key,val in ns.items():
        if isinstance(val, Dict):
            ns[key] = val.__dict__
    for bt in ["s_buffs","t_buffs"]:
        if hasattr(skill, bt):
            ns[bt] = [DB["Buff"][b].__dict__ for b in getattr(skill, bt)]
    if LOC[iname]: 
        ns.update(LOC[iname])
    try:
        json.dumps(ns)
    except TypeError:
        print()
    comp.append(ns)

with open("skill_buffs.json","wt",encoding="utf8") as f:
    json.dump(comp, f, ensure_ascii=False, indent="\t")
